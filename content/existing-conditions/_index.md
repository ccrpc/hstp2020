---
title: "Existing Conditions"
draft: false
menu: main
weight: 15

bannerHeading: Existing Conditions
bannerText: These data points build an understanding of the current context from which future plans can develop.
bannerUrl: banner

---

## Purpose and Mission
The Region 8 HSTP provides a comprehensive review of existing public transportation and human service coordination, a foundation for continued communication between human service agencies and transportation providers, and identification of strategies to address unmet transportation needs within the community. Transportation providers and human service agencies can use the plan to identify coordination opportunities and areas for improvement. The study area for this plan includes Champaign, Clark, Coles, Cumberland, DeWitt, Douglas, Edgar, Macon, Moultrie, Piatt, Shelby, and Vermilion Counties in East Central Illinois.

(map of Region 8 with urbanized area and highways)

## Policy and Technical Committees
The Region 8 Policy and Technical Committees are responsible for the approval and adoption of this plan as it relates to the counties of Champaign, Clark, Coles, Cumberland, DeWitt, Douglas, Macon, Moultrie, Piatt, Shelby, and Vermilion in East Central Illinois. These committees are governed by bylaws which were first established as operating procedures in 2007 before being adopted as bylaws in 2011 per suggestion from IDOT, and most recently updated in 2015.

Region 8 convenes quarterly for a combined Policy and Technical Committee meeting; the June meeting each year serves as the annual business meeting to vote for Committee Chairs when applicable, nominate new members, and address other business items as needed. All meeting agendas and minutes are posted on the Region 8 HSTP website in compliance with the Illinois Open Meetings Act (OMA).

### Representation

The Policy Committee consists of one appointed representative from each Region 8 county (with the exception of DeWitt and Edgar at the time of this plan’s adoption), who act as liaisons for their respective county governments. At the time of the adoption of this plan, the Region 8 Policy Committee consists of one member representing each of the following entities:

- Champaign County Board
- Coles County Board
- Cumberland County Board
- Douglas County Board
- Macon County Board
- Moultrie County Board
- Piatt County Board
- Shelby County Board
- Vermilion County Board

The Technical Committee consists of representatives from rural public transportation agencies that receive Section 5311 funding, human service agencies that receive Section 5310 funding, and other voting members representing consumers who use public and/or human service transportation. The Region 8 Technical Committee is represented by members of the following entities:

#### Rural Public Transportation Providers (Section 5311 Recipients)

- Champaign County Area Rural Transit System (C-CARTS)
- Central Illinois Public Transportation (CIPT)
- CRIS Rural Mass Transit District (CRIS RMTD)
- Coles County Dial-A-Ride (DAR)
- Piatt County Public Transportation (Piattran)
- Rides Mass Transit District (Rides MTD)
- SHOW BUS Public Transportation

#### Human Service Agencies (Section 5310 Recipients)

- CCAR Industries
- Charleston Transitional Facility
- Crosspoint Human Services
- Developmental Services Center
- Human Resources Center of Edgar & Clark Counties
- LifeLinks, Inc.
- Macon Resources, Inc.
- Marion County Horizon Center
- Moultrie County Beacon
- Shelby County Community Services
- Swann Special Care Center

#### Other Voting Members

- East Central Illinois Area Agency on Aging (ECIAAA)
- Health Alliance
- Soyland Access to Independent Living (SAIL)

Region 8 also has an Advisory Committee, composed of one representative from each of the Metropolitan Planning Organizations within the region: Champaign County Regional Planning Commission (CCRPC), Danville Area Transportation Study (DATS), and Decatur Urbanized Area Transportation Study (DUATS). Advisory Committee members are not required to attend regular meetings, but are encouraged to attend when topics of discussion pertain to the urbanized areas.

<rpc-table url="hstpmembers_table.csv"
  table-title="Technical Committee Representation"
  text-alignment="l,r"></rpc-table>


#### Coordination and Planning Process

The diverse background of committee members and stakeholders in Region 8 fosters a well-rounded planning process and meaningful dialogue for coordination efforts. Although the agencies provide a wide variety of services with different funding regulations and requirements, the core mission of providing the highest quality service and experience to the end-user unifies this group. Transportation is a vital component across the board, as either the primary function or a necessary means to delivery of the primary function of each agency.

Quarterly meetings of the HSTP Region 8 Policy and Technical Committees serve as the primary source of feedback tied to the planning process. Public providers and human service agencies discuss current services, voice concerns and difficulties, offer advice, identify coordination opportunities, and develop a vision for future conditions through discussion at the quarterly meetings.

During the quarterly meeting in December 2020, the Policy and Technical Committees approved new short-term goals and objectives, and in September 2020 the committees approved long-term goals to be included in the 2020 Region 8 HSTP.

## Public Participation Efforts

Opportunities for public participation exist in many forms throughout Region 8. Many counties have transportation advisory groups or interagency coalitions whose meetings are open to the public. Some of these advisory groups were initiated during the Interagency Coordinating Committee on Transportation (ICCT) Primer Process. All Region 8 HSTP meetings are open to the public, and consumers from participating human service agencies are encouraged to attend, however public participation is rare.

The Illinois State Plan for Independent Living (SPIL) 2021-2023 includes a transportation element, including an objective to increase the capacity of Centers for Independent Living (CILs) to provide services and advocacy for people with disabilities. To achieve this objective, the Statewide Independent Living Council (SILC) have, and will continue to, host an annual meeting between transportation providers and/or coordinators, Rural Transit Assistance Center (RTAC), and CIL staff to exchange information and ideas to further advance transportation services for people with disabilities. Attendees provide informal feedback to the HSTP Coordinators on outreach methods and best practice policies for transportation.

### Consumer Surveys

In addition to public participation at meetings, recipients of Section 5310 and Section 5311 funding are required by these programs to conduct annual surveys to collect input from their consumers. These survey results are used by each agency to evaluate existing service and develop targets for improvement.

From a regional perspective, the survey results are not extremely useful as the questions vary from provider to provider. To address this, the Region 8 Technical and Policy Committees approved a uniform survey to be administered across the region. For this update of the Region 8 HSTP, the first round of uniform consumer surveys were distributed to all 5310 and 5311 providers represented on the Technical Committee, in an effort to assess satisfaction, obstacles, mobility barriers, and unmet needs across the region.


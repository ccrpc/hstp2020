---
title: "Mobility Today"
draft: false
weight: 50
---

### Existing Transportation Services

The Region 8 transportation network is composed of a variety of public transit, human service transportation, and private transportation providers. The following inventory is an attempt to list all of the known transportation options available to seniors, persons with disabilities, and persons with low-income. The inventory identifies each agency and provides a brief description of the service, eligibility information, and service area.

#### Rural Public Transportation
Region 8 is currently served by seven rural public transportation entities: C-CARTS (Champaign), Central Illinois Public Transit (Moultrie and Shelby), Coles County Dial-A-Ride (Coles and Douglas), CRIS Rural Mass Transit District (Vermilion), Piatt County Rural Public Transportation (Piatt), Rides Mass Transit District (Clark, Cumberland, Edgar), and SHOWBUS Public Transportation (DeWitt and Macon). These providers operate demand-response service, and most operate fixed-routes as well. Service areas for Region 8 rural transit operators are shown below.

Statewide, there are several rural 5311 providers that operate human service transportation under Section 5310 in addition to general public service. In HSTP Region 8, C.E.F.S. Economic Opportunity Corporation/Central Illinois Public Transit (CEFS/CIPT) is the only 5311 provider operating under these conditions.

#### Urbanized Area Public Transportation
Section 5307 providers operate fixed-route general public transportation with complementary paratransit services for areas with populations over 50,000. Although this plan focuses on the non-urbanized areas in Region 8, the three urbanized area systems are part of the overall transportation network, and rural residents benefit from connections made with these providers. Champaign-Urbana Mass Transit District (CUMTD) serves the Champaign-Urbana Urbanized Area, Danville Mass Transit (DMT) serves the Danville Urbanized Area, and Decatur Public Transit System (DPTS) serves the Decatur Urbanized Area.

{{<image src="region8map.jpg"
  link="/destination"
  caption="Map of Region 8 by county and transportation providers"
  alt="Map of Region 8 by county and transportation provider"
  position="left">}}

#### Human Service Transportation Programs
Region 8 has a diverse network of non-profit organizations that provide transportation as a secondary service for consumers who participate in their human or social service programs for a variety of purposes. These services are specifically shaped by the consumers’ transportation needs that public and private providers are not able to meet for a variety of reasons. Generally, consumers are transported to-and-from the agency for day programs, to-and-from coordinated employment, or transportation is provided for community outings. These non-profit agencies that receive vehicles through the Consolidated Vehicle Procurement (CVP) are required to participate in the HSTP process.

#### Private and Other Transportation
Private transportation services are typically for-profit companies such as taxis and intercity buses (Greyhound, Peoria Charter, etc.). Intercity bus and Amtrak routes are shown in the map below. Other services include transportation for K-12 and higher education. The majority of school districts in Region 8 provide their own transportation with traditional yellow school buses, as indicated in the talbe below. Transportation services for higher education institutions are also listed below for Region 8.

<rpc-table url="transportation_providers_chart.csv"
  table-title="Region 8 Transportation Providers"
  text-alignment="l,r"></rpc-table>


*insert map of private intercity transportation amtrak, burlington, etc. here*

<rpc-table url="k12transportation.csv"
  table-title="Contracted K-12 Transportation"
  text-alignment="l,r"></rpc-table>

  <rpc-table url="higher_education_transportation.csv"
    table-title="Transportation Options for Higher Education"
    text-alignment="l,r"></rpc-table>

#### Region 8 Transportation Directory
For a complete list of Region 8 providers, transportation services, contact information, and fares, please refer to the Region 8 HSTP Transportation Directory, found online at https://ccrpc.org/data/hstp-region-8-directory/.

### Coordination and Successes

Coordination between providers in all sectors of transportation allows the region to leverage resources and services to provide the most efficient, effective transportation options for the end-user. Some common occurrences of coordination and success in Region 8 are listed below:

Many providers are using the Regional Maintenance Center in Springfield for large repairs, and its loaner vehicle program to avoid lapse in service.

Human service agencies are contracting an increasing number of routes to rural providers, and in some cases the human service agency leases a 5310 vehicle to the rural provider for those routes.

Several human service agencies and public providers have agreements with local emergency management agencies to provide vehicles for evacuation or other emergency response.

Coordination between public providers has increased over the years, for example: multiple 5311 providers coordinate to transfer passengers between counties, rural providers notify other rural providers of long-distance trips and work to accommodate out-of-town passengers on these trips, and some 5311 agencies suggest other rural public providers for service contracts they are not able to accommodate.

Although some characteristics are applicable to Region 8 as a whole, each county has unique advantages, circumstances, and obstacles. This section offers a snapshot of each county’s coordination and successes. Examples include progress toward/achievement of HSTP goals, new/increased services, involvement in interagency groups, service contracts, supplemental transportation, specialized transportation, sharing or contracting resources, and mobility management.

#### Champaign County
* The Champaign-Urbana Mass Transit District (MTD) operates both urbanized and rural transit, creating a cohesive transportation network.
* The Champaign County Rural Transit Advisory Group (RTAG) meets quarterly and is composed of a representative for older adults, persons with low-income, persons with disabilities, medical, education, employment, and the county board. C-CARTS staff facilitates these meetings, reports on service, and updates the RTAG on relevant matters.
* The Senior Resource Center at Family Service of Champaign County provides a volunteer transportation service that coordinates with the Champaign County Senior Task Force and MTD.
* C-CARTS offers discounted fares to seniors and persons with disabilities.
* C-CARTS has an informal service contract with the Eagle’s View assisted living in Rantoul to provide supplemental transportation to its residents when needed.
* Developmental Services Center (DSC) has a service contract with Piattran to bring consumers from Piatt County to services in Champaign.
* In November 2016 C-CARTS introduced the Eagle Express, a deviated-fixed route in the Village of Rantoul, with a connecting, commuter route to Champaign-Urbana. This connecting route includes a stop at the Illinois Terminal, where riders can connect with MTD, Greyhound, Amtrak, and the fixed-route to Danville.
* Danville Mass Transit (DMT) provides a fixed-route between Danville and Champaign-Urbana Monday through Saturday.
*	C-CARTS, Danville Mass Transit District, and CRIS Rural Mass Transit are on Google Transit to more easily connect trips from  Champaign County to Vermillion County, as well as the Champaign-Urbana urbanized area (served by MTD).

#### Clark County

* Rides MTD is Medicaid-certified and accepts electronic payment (First Transit) for medical trips.
* Rides MTD offers discounted fares to seniors and persons with disabilities.
* Rides MTD offers cost savings through punch cards and monthly passes.
* Rides MTD provides transportation to and from Terre Haute, Indiana Monday through Friday.
* Rides MTD has many routes connecting Clark and Edgar Counties.
* Rides MTD operates daily routes that connect riders to the regional and national Greyhound intercity bus network.
* Rides MTD provides transportation to universities and hospitals in Indiana.
* Rides MTD provides transportation to and from Charleston/Mattoon Monday, Wednesday, and Friday.
* Residential reservations only require a one-hour call-ahead.
* Rides MTD meets quarterly with the Clark County Advisory Group.
* Rides MTD coordinates trips with Lake Land Community College Eastern Region Center Extension in Marshall.


#### Coles County

* Dial-A-Ride is Medicaid-certified and accepts electronic payment (First Transit) for medical trips.
* Dial-A-Ride offers a discounted fare to seniors.
* Dial-A-Ride provides cost savings for all riders through a monthly pass, which is further discounted for active military and veterans.
* Dial-A-Ride provides scheduled service to Champaign-Urbana every Monday.
* Dial-A-Ride provides scheduled service to Effingham every Wednesday.
* Dial-A-Ride provides scheduled grocery store routes in Charleston every Monday, Wednesday, and Thursday.
* Dial-A-Ride provides scheduled service to Douglas County on Tuesdays and Thursdays.
* Dial-A-Ride provides scheduled grocery store routes in Mattoon every Tuesday through Thursday, and to the Cross County Mall every Friday.
* Dial-A-Ride operates routes that connect riders to the regional and national Greyhound intercity bus network as well as Amtrak commuter rail.
* Dial-A-Ride runs multiple routes for CCAR Industries through service contracts.
* Dial-A-Ride coordinates transportation to LifeLinks, Peace Meal, Coles County Council on Aging, the Coles County Health Department, Sarah Bush Lincoln, and Lake Land Community College.
* Dial-A-Ride provides Head Start transportation for Coles County.
* Dial-A-Ride and CCAR Industries have a working agreement with ESDA to share vehicles for emergency events.
* Rides MTD brings consumers to CCAR from Clark, Cumberland, and Edgar Counties.
* CCAR Industries provides supplemental transportation for the Charleston Park District and Sarah Bush Lincoln.
* Dial-A-Ride runs a Zip Line service at a reduced rate with free transfers at the LifeSpan Center.
* CTF Illinois utilizes Dial-A-Ride’s Zipline for work trips.
* Dial-A-Ride provides frequent transportation to some of CTF’s clients who have become comfortable with the service.
* Dial-A-Ride's fixed routes in Coles County are displayed on Google Transit for passengers to plan trips via app or website.


#### Cumberland County

* Rides MTD is Medicaid-certified and accepts electronic payment (First Transit) for medical trips.
* Rides MTD offers discounted fares to seniors and persons with disabilities.
* Rides MTD offers cost savings through punchcards and monthly passes.
* Rides MTD regularly participates in the interagency coalition meetings.
* Rides MTD provides transportation for the Life Center of Cumberland County.
* Rides MTD operates daily routes that connect riders to the regional and national Greyhound intercity bus network.
* Residential reservations only require a one-hour call-ahead.

#### DeWitt County

* SHOW BUS actively participates in the human service interagency coalition.
* Faith in Action coordinates with SHOWBUS.
* SHOW BUS travels to Champaign once per week, Decatur twice per week, and Bloomington two times per week.
* SHOW BUS is Medicaid-certified and accepts electronic payment (First Transit) for medical trips.
* SHOW BUS provides transportation to seniors at no cost.
* SHOW BUS offers a Voucher Program which reimburses not-for-profit agencies that are assisting individuals with especially difficult mobility obstacles (such as the fare for a trained professional to accompany the rider).

#### Douglas County

* Dial-A-Ride is Medicaid-certified and accepts electronic payment (First Transit) for medical trips.
* Dial-A-Ride provides transportation to seniors within Douglas County at no cost.
* Dial-A-Ride provides cost savings for all riders through a monthly pass, which is further discounted for active military and veterans.
* Dial-A-Ride provides scheduled service to Champaign-Urbana every Monday and Friday.
* Dial-A-Ride operates routes that connect riders to the regional and national Greyhound intercity bus network as well as Amtrak commuter rail.
* Douglas County has experienced increased ridership and opportunity for growth since Dial-A-Ride began operating the service July 1, 2015.
* Dial-A-Ride provides transportation for the Douglas County Mental Health Center, Health Department, and Head Start.
* Dial-A-Ride regularly meets with their advisory group.
* Dial-A-Ride provides scheduled service to Decatur every Wednesday.
*Dial-A-Ride provides scheduled service Coles County every Tuesday and Thursday.
* Dial-A-Ride provides transportation to Piatt County Mental Health day program for Douglas County residents.

#### Edgar County

* Rides MTD is Medicaid-certified and accepts electronic payment (First Transit) for medical trips.
* Rides MTD offers discounted fares to seniors and persons with disabilities.
* Rides MTD offers cost savings through punch cards and monthly passes.
* Faith in Action of Edgar County coordinates with Rides MTD.
* Residential reservations only require a one-hour call-ahead.
* Rides MTD acquired a new operations center in Paris in 2014.
* Rides MTD operates daily routes that connect riders to the regional and national Greyhound intercity bus network.
* Rides MTD has many routes connecting Clark and Edgar Counties.

#### Macon County

* SHOW BUS travels to both Champaign and Springfield twice per month.
* SHOW BUS took over transportation for St. Mary’s Hospital.
* Richland Community College has a diesel mechanic and hydraulic lift training program.
* Macon County Rural Transit Advisory Group meets quarterly and has the involvement of county board members, Macon Resources, Macon County Resource Center, Macon County Health Department, Soyland Access to Independent Living, SHOW BUS, and other stakeholders.
* Decatur Public Transit System and SHOW BUS provide supplemental transportation for Macon Resources, Inc.
* SHOW BUS is Medicaid-certified and accepts electronic payment (First Transit) for medical trips.
* SHOW BUS provides transportation services to seniors at no cost.
* SHOW BUS offers a Voucher Program which reimburses not-for-profit agencies that are assisting individuals with especially difficult mobility obstacles (such as the fare for a trained professional to accompany the rider).

#### Moultrie County

* CIPT is Medicaid-certified and accepts electronic payment (First Transit) for medical trips.
* CIPT provides transportation services to seniors at no cost.
* CIPT offers cost savings through monthly passes.
* CIPT meets regularly with their transportation advisory group.
* Moultrie County Beacon lends private groups (churches, theater, etc.) vehicles when needed.
* CIPT provides transportation to and from daycares, after school programs, and summer food programs.
* CIPT uses volunteer aides for passenger assistance, which counts as in-kind contribution.


#### Piatt County

* Piattran is Medicaid-certified and accepts electronic payment (First Transit) for medical trips.
* Piattran offers discounted fares to seniors, students and persons with disabilities.
* Piattran offers cost savings through an Autopay program where the riders provides us with a credit card or bank account to keep on file and charge the exact amount for rides taken each month.  
* Piattran travels to Champaign-Urbana and Decatur multiple times per day.
* Trips to Springfield, Peoria, Arcola, Arthur, and Tuscola and others are available on a monthly basis through the Piatt County Senior Transportation program.
* Piattran participates in a transportation subcommittee that reports to the County Board.
* Piattran partners and provides transportation for the Piatt County Senior Citizens Transportation Program, Piatt County Nursing Home, Piatt County Mental Health Center, HeadStart in Cisco, Developmental Service Center in Champaign, Monticello Community School District for their Special Needs program, Yzone and youth program by the YMCA, and Kirby Hospital.
* Piattran coordinates with SHOWBUS and C-Carts when possible.
* Registered seniors ride scheduled in-county routes for free, and are allowed one scheduled route out-of-county round-trip per month at no cost.


#### Shelby County

* CIPT is Medicaid-certified and accepts electronic payment (First Transit) for medical trips.
* CIPT provides transportation services to seniors at no cost.
* CIPT offers cost savings through monthly passes.
* Shelby County Community Services leases three vehicles to Central Illinois Public Transit.
* CIPT meets regularly with their transportation advisory group.
* CIPT provides transportation to and from daycares, after school programs, and summer food programs.
* CIPT uses volunteer aides for passenger assistance, which counts as in-kind contribution.
* Shelby County Community Services leases three vehicles to CIPT to run SCCS’s out-of-town routes.
* SCCS coordinates with the nursing home and Shelby County Sheriff’s Department.

#### Vermillion County

* CRIS offers Saturday service from 8:00 a.m. to 4:00 p.m.
* CRIS operates Paratransit service for Danville Mass Transit.
* CRIS provides supplemental transportation for Crosspoint Human Services’ day program.
* CRIS is Medicaid-certified and accepts electronic payment (First Transit) for medical trips within Vermilion County.
* CRIS provides MCO rides for Vermilion or Champaign County riders.
* One-way fares are $1.00 for all riders.
* CRIS participates in the Technical Committee for HSTP Region 8 and the Danville Area Transportation Study (DATS).
* Danville Mass Transit (DMT) provides a fixed-route between Danville and Champaign-Urbana Monday through Saturday.
* The Hoopeston Multi-Agency Service Center utilizes CRIS as a back-up transportation option for its programs.
* CRIS provides transportation to-and-from court appearances as needed, and the county board pays for the trips.
* CRIS participated in the planning process for the 2025 Vermilion County Plan.


### Regional Program of Projects

The tables below outline the awards and denials for 5310 and 5311 grantees in Region 8 through the Consolidated Vehicle Program (CVP) with the Illinois Department of Transportation (IDOT). The project cost is determined by the price of each vehicle type. Grantees may apply for minivans, light duty, medium duty, and super-medium duty buses.

<rpc-table
url="region-8-rpop-5310.csv"
table-title="Regional Program of Projects for 5310 Grantees"
source="CCRPC and IDOT-OIPI">
</rpc-table>

<rpc-table
url="region-8-rpop-5311.csv"
table-title="Regional Program of Projects for 5311 Grantees"
source="CCRPC and IDOT-OIPI">
</rpc-table>

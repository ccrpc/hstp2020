---
title: "Performance Measures Regional Overview"
draft: false
weight: 70
---
## Region 8 Report Card

### About the HSTP Report Card

The Region 8 Human Service Transportation Plan (HSTP) Technical and Policy Committees approved the updated HSTP on December 14th, 2017. The goals outlined in the HSTP serve as a guidepost for HSTP activities and are tracked for progress.

The data measured in this report card came from HSTP agencies through self-reported data requests. In this report, public transit agencies are also referred to as “5311 agencies,” because of the federal funding name. Human service agencies are subsequently referred to as 5310 agencies.   

The 2017 HSTP goals were formulated into performance measures that are already tracked by the agencies, including vehicle miles traveled, trips, and denials, among other metrics. Some sections, such as vehicle hours, have incomplete data since several 5310 agencies do not track this information.  

This report card will compare data from the previous 2017 baseline year as a baseline for future HSTP data analysis and discusses some of the performance measures that have been consistently tracked. Several metrics have been collected previously through the Rural Mobility Index, a detailed record of Illinois rural transit statistics compiled by the Illinois Institute for
Rural Affairs. Data is assigned a positive, neutral, or negative rating depending on the data trend.  

The HSTP Technical Committee is comprised of administrative staff at human service
agencies, health organizations, and public transit providers who receive 5310 funding from the Illinois Department of Transportation. The Policy Committee members are county board members who are appointed by their respective county board chair.

### Report Card Participants

The following agencies submitted data used to create this report card:

#### Human Service (5310) Agencies
* CCAR Industries
* Crosspoint Human Services
* CTF Illinois
* Developmental Services Center
* Human Resources Center of Edgar & Clark
* LifeLinks Inc.
* Macon County Resources
* Moultrie County Beacon
* Shelby County Community Services

#### Public Transit (5311) Agencies
* Champaign County Area Rural Transit System (C-CARTS)
* Central Illinois Public Transit
* CRIS Rural Mass Transit District
* Dial-A-Ride
* Piattran
* Rides Mass Transit District
* Showbus

#### Other Agencies
* Champaign County Regional Planning Commission (CCRPC)
* Rural Transit Assistance Center (RTAC)


### How to Access the Region 8 HSTP Report Card

The report card can be found on the [Champaign County Regional Planning Commission](https://ccrpc.org/) website and also in the link below.

[Region 8 Report Card](https://ccrpc.org/documents/region-8-hstp-plan-2017/2018-region-8-hstp-report-card/)  

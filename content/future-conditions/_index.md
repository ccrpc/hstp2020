---
title: "Future Conditions"
draft: false
menu: main
weight: 20

bannerHeading: Future Conditions
bannerText: Using data from transit riders across Region 8, this page will outline goals and recommendations for the future.
bannerUrl: banner
---
Our region’s transportation landscape has changed significantly since the Covid-19 pandemic. Transportation agencies are confronting challenges with hiring and retention of vital operators and staff, navigating new regulations and sources of funding, and experiencing unprecedented levels of stress and threats to our collective health. Yet, demand for affordable and reliable transportation has remained constant. Planning for the future of mobility in Region 8 is more critical than ever. Using data from transit riders across Region 8, this page will outline goals and recommendations for the future.


---
title: "Introduction"
draft: false
menu: main
weight: 10

bannerHeading: Introduction
bannerText: As a collaborative transportation policy document, the HSTP provides a regional vision to guide future transportation investments.
bannerUrl: banner
---
The Human Service Transportation Plan (HSTP) is an initiative to evaluate existing transportation services, identify the transportation needs of individuals with disabilities, older adults, and people with low incomes, and establish goals and strategies for meeting these needs in Champaign, Clark, Coles, Cumberland, DeWitt, Douglas, Edgar, Macon, Moultrie, Piatt, Shelby, and Vermilion Counties in Illinois. The HSTP program was originally instated in 2005 as part of the Safe Accountable, Flexible, Efficient Transportation Equity Act: A Legacy for Users (SAFETEA-LU), which guaranteed funding for highways, highway safety, and public transportation.

As a requirement of SAFETEA-LU, grantees under Section 5310: Enhanced Mobility of Seniors and Individuals with Disabilities, Section 5316: Job Access and Reverse Commute (JARC), and Section 5317: New Freedom Program were required to be under a “locally developed coordinated public transit human services transportation plan” (HSTP) to be eligible for transportation funding for federal fiscal year 2007 and beyond. Over the years, Sections 5316 and 5317 have been rolled into urban and rural formula grant programs, and the HSTP program has been continued by each federal transportation bill as a means of enhancing access and quality through coordination.

Each state receives Federal Transit Administration (FTA) funds for the programs subject to the HSTP coordination process, and each state is required to determine how best to meet the mandate for coordination. The Illinois Department of Transportation – Office of Intermodal Project Implementation (IDOT-OIPI), formerly the Department of Public and Intermodal Transportation (DPIT), established eleven HSTP regionsand a coordinator for each. The Champaign County Regional Planning Commission (CCRPC) is the Metropolitan Planning Organization (MPO) for the Champaign-Urbana Urbanized Area. CCRPC staff is required to develop the Champaign-Urbana Urbanized Area HSTP, and facilitates the same process for HSTP Region 8.

## Major Plan Components
The Region 8 Human Service Transportation Plan consists of six major components:

- An overview of current transportation legislation and funding;

- Public participation and the planning process;

- Existing conditions and demographics of the region;

- A view of mobility today, analyzing major trip generators, top employers, and existing transportation services within the region;

- Service coordination, gaps, unmet needs, and duplication; and

- A vision for mobility tomorrow, identifying goals and objectives for the region.

## Transportation Legislation and Funding

### MAP-21

On July 6th, 2012, SAFETEA-LU was replaced with the authorization of a two-year federal transportation bill named Moving Ahead for Progress in the 21st Century (MAP-21), covering federal fiscal years 2013 and 2014. MAP-21 reaffirmed the statute mandating local coordination of transportation services, and consolidated some of the funding programs affected by these requirements. Job Access and Reverse Commute (JARC), formerly Section 5316, no longer exists as a separate program, but funding for these activities is available under both Urbanized Area Formula Grants (Section 5307) and Formula Grants for Rural Areas (Section 5311); and New Freedom, formerly Section 5317, was absorbed by Section 5310. As a result, Section 5307 and Section 5311 providers were required to participate in the HSTP process. MAP-21 was originally set to expire September 30th, 2014, however five extensions allowed the bill to remain in effect until December 4th, 2015.

### FAST Act: Current Funding

The five-year Fixing America’s Surface Transportation Act Bill (FAST Act) for federal fiscal years 2016 through 2020 was authorized by President Obama on December 4th, 2015. The FAST Act includes roughly one billion dollars per year in increases across all transit funding streams. The FAST Act also reintroduced a Discretionary Bus and Bus Facilities Program (Section 5339), available to 5307 and 5311 recipients.

#### Section 5310 Changes and Continued Funding Status

Enhanced Mobility of Seniors and Individuals with Disabilities (Section 5310) provides funding for programs beyond traditional public transportation and ADA paratransit service to meet the specific needs of seniors and persons with disabilities. Section 5310 remains largely unaltered by the FAST Act. A minimum of 55% of funds must be allocated for capital projects such as the procurement of ADA accessible buses and vans, vehicle maintenance, purchase of service, computer hardware and software, etc. The other 45% of program funds may be used for other projects, such as those originally targeted by the New Freedom program: travel trainings, sidewalks, improved signage, way-finding technology, etc. The goals of Section 5310 are to maintain a safe fleet of vehicles to service transportation needs of the indicated target populations, to support the continuation and growth of existing services, and foster the growth of new services.

Funding is allocated to state Departments of Transportation (DOTs) for rural and small urban (population under 200,000) based on each state’s population of the two target groups for this program. For large urbanized areas (population over 200,000), the Governor selects a designated direct recipient. In Illinois Section 5310 funding is primarily used to finance the Consolidated Vehicle Procurement (CVP) program, providing vehicles at no cost to the grantee; funded by 80% federal funds with a 20% state match. Subrecipients of these funds within Region 8 include local government authorities that operate public transit, rural mass transit districts, and private non-profit organizations.

Although Section 5310 has been largely unaltered by the FAST Act legislation, one notable provision, Section 3006 of the Act, created a discretionary “pilot program for innovative coordinated access and mobility,” which opens up funding for innovations in coordination of transportation for disadvantaged populations, Non-Emergency Medical Transportation (NEMT) services, and coordination technology such as one-call or one-click centers. This provision also calls on the federal interagency Coordinating Council on Access and Mobility (CCAM) to create and update a strategic plan on transportation coordination between federal agencies, including proposed changes to federal laws and regulations that currently hinder transportation coordination at the local level.

#### Section 5311 Changes and Continued Funding Status

Formula Grants for Rural Areas (Section 5311) is an FTA program that allocates funds to states for the purpose of supporting rural public transportation, defined as areas with a population under 50,000. Section 5311 has been largely unaltered by the FAST Act. The formula is based on population, square miles of service area, revenue vehicle miles, and low-income population. The objectives of Section 5311 are to provide rural residents with enhanced access to jobs, medical services, education, and other opportunities concentrated in urbanized areas; assist in the maintenance, development, improvement, and use of public transit in non-urbanized communities; encourage and facilitate the most efficient use of all transportation funds to provide passenger transportation in rural areas through the coordination of programs and services; and assist in the development of intercity bus transportation, (each state must commit no less than 15% of its annual 5311 funds to intercity bus service).

Section 5311 provides capital, planning, and operating assistance, including funding for projects previously available through JARC. Under the FAST Act, Section 5311 recipients are now able to utilize revenue from advertisement and concessions as local match, allowing for the spend-down of a larger percentage of the apportionment. In Region 8, there are currently seven rural public transportation entities that utilize Section 5311 funds.

## Urbanized vs. Rural Funding Eligibility

Section 5307, Urbanized Area Formula Grants, serves the same purpose as Section 5311, but for areas with 50,000 or more residents and designated as “urbanized areas” by the United States Census Bureau. The three urbanized areas within Region 8 are Champaign-Urbana, Danville, and Decatur; all considered small urbanized areas (under 200,000 population). In these cases, FTA funds are distributed to the Governor, and apportioned to the subrecipient public transportation providers: Champaign-Urbana Mass Transit District (CUMTD), Danville Mass Transit (DMT), and Decatur Public Transit System (DPTS) respectively.

These agencies submit requisitions and coordinate vehicle purchases directly with the FTA; however, all reporting must be submitted to IDOT, and these agencies are subject to compliance reviews conducted or contracted by IDOT. JARC and New Freedom type projects must be applied for through the respective Metropolitan Planning Organization (MPO) as part of the Urbanized Area HSTP coordination process, and included in the Transportation Improvement Plan (TIP) if awarded.

In an effort to facilitate connections between Urbanized and Rural systems, Section 5311(f) Intercity Bus funding is available to rural transit providers, private motor coach, and rail providers for projects that connect rural communities with cities and opportunities to travel to further destinations.

## State Funding and Local Match

The State of Illinois provides state funding for all public transportation providers, regardless of population, in the form of Downstate Operating Assistance Program (DOAP) funds. DOAP provides assistance to recipients to assist in the operation and improvement of public transportation services in the urban and rural areas of downstate Illinois. For most operators, DOAP is the primary source of reimbursement for operating and administrative expenses. Rural transit agencies utilize Section 5311 dollars as local match for DOAP. Another form of local match available to public transit operators is called Transportation Development Credit (TDC), formerly known as Toll Revenue Credit (TRC). These credits are distributed by the FTA to states based on actual expenditures made by state toll authorities to build and maintain critical transportation infrastructure.

## Other Transportation Funding

In addition to funding mechanisms dedicated specifically to transit, numerous funding sources exist at various federal, state, and local levels through a number of programs and initiatives that may be applied to transportation services.

### Social Security Act Title XIX - Medicaid Transportation Funding

The Illinois Department of Healthcare and Family Services (IDHFS) contracts with First Transit, Inc. to provide the Non-Emergency Transportation Services Prior Authorization Program (NETSPAP) and brokerage for Medicaid funded transportation. IDHFS maintains the requirements and regulations for transportation providers to become Medicaid certified, and First Transit is the call center that approves all transportation funded by Medicaid. This funding becomes particularly vital in rural transit due to long-distance trips for specialized medical services, as these span multiple service areas in many cases.

### Older Americans Act Title IIIB - Area Agency on Aging Transportation Funding

The federal Older Americans Act of 1965 (OAA) provides funding for a variety of in-home and community-based services to enhance quality of life, maintain independence, and assist with aging in place. A prime contributor to the success of these goals is transportation. Title I and Title II of the OAA declare the objectives and create the Administration on Aging (AOA), and Title III establishes Grants for State and Community Programs on Aging including transportation. Funding for Title III programs is distributed to states based on population of individuals over the age of 60. It is then up to each state to apportion its funding to area agencies on aging. President Obama signed the Older Americans Act Reauthorization into law April 19th, 2016, guaranteeing funding through FFY2019.
HSTP Region 8 is entirely serviced by the East Central Illinois Area Agency on Aging (ECIAAA). Transportation falls under ECIAAA’s Access Services, defined as “a network of 12 Coordinated Points of Entry to provide information and assistance; and coordination with 6 Care Coordination Units and public and private transportation providers.”

### Service Contracts and Associated Human Service Program Funding

Human service agencies serve vital direct-service roles for their consumers, but are sometimes also compelled to provide transportation services in order to get their consumers to and from their agencies. Ideally, service agencies would spend their time solely providing direct-service and transportation providers would transport the consumers. The primary focus of the HSTP is to increase coordination between public transportation providers and human service agencies so consumers have efficient, affordable, and quality access to services and agencies they need. In addition to the benefits for consumers, service contracts are fiscally advantageous to service providers on both sides. While the human service agency benefits in cost-savings and/or convenience, the public transit operator is able to show increased ridership and use contract revenue as local match to access more Downstate Operating Assistance Program (DOAP) funding. Additional benefits include:

- Pooling resources for a reduction in underutilization;
- Utilization of economies of scale for increased efficiency;
- Elimination of unnecessary competition for scarce resources;
- Better use of deadhead time;
- Attainment of skills or services without long-term commitment;
- Solution to agency limitations;
- Overall reduction in transit system cost per trip; and

Ability of human service agencies to spend more time on core services.
Human service agencies serve vital direct-service roles for their consumers, and aside from community outing trips, these agencies provide transportation out of necessity to get consumers to and from services. In an ideal situation, service agencies would spend their time solely providing direct-service, and transportation would be provided by agencies whose specialty is transportation. For many reasons discussed later in the plan, this is not possible to the ideal degree however, steps can be made toward achieving this relationship where possible. While the human service agency benefits in cost-savings and/or convenience, the public transit operator is able to show increased ridership and use contract revenue as local match to draw down DOAP funding.
